import React, { Component } from 'react';

class FormularioEditar extends Component {

    titulo      = React.createRef();
    contenido   = React.createRef();
    id          = React.createRef();

    editarPost = (e) => {
        e.preventDefault();
        let titulo      = this.titulo.current.value;
        let contenido   = this.contenido.current.value;
        let post = {
            id: this.props.post.id,
            title: titulo,
            body: contenido,
            userId: 1
        }
        this.props.editarPost( post );
        // e.currentTarget.reset();
    }

    getPost = () => {
        if(!this.props.post) return null;
        const {title, body} = this.props.post;
        return (
            <form className='col-8' onSubmit={this.editarPost}>
                <legend className='text-center'> Editar nuevo post </legend>
                <div className='form-group'>
                    <label> Titulo: </label>
                    <input ref={this.titulo} defaultValue={title} type='text' placeholder='Título del post' className='form-control' />
                </div>
                <div className='form-group'>
                    <label> Contenido: </label>
                    <textarea ref={this.contenido} defaultValue={body} className='form-control' placeholder='Contenido del post...'></textarea>
                </div>
                <input type='submit' className='btn btn-primary' value='Editar' />
            </form>
        );
    }

    render() {
        return (
            this.getPost()
        );
    }
}

export default FormularioEditar;