import React, { Component } from 'react';
import '../styles/detalle_post.css';

class DetallePost extends Component {

    detalle = (props) => {
        if(!props.post) return null;
        const {id, title, body, userId} = this.props.post;
        return (
            <React.Fragment>
                <h4> {id}.- {title} </h4>
                <p> Autor:  <span> {userId} </span> </p>
                <p> Cuerpo: <span> {body} </span> </p>
            </React.Fragment>
        );
    }

    render() {
        return (
            <div className='col-12 col-md-8'>
                { this.detalle( this.props ) }
            </div>
        );
    }
}

export default DetallePost;