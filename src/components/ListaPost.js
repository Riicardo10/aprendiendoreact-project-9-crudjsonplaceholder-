import React, { Component } from 'react';
import Post                 from './Post';

class Posts extends Component {

    showLista = () => {
        return (
            <React.Fragment>
                { Object.keys( this.props.posts ).map( post => {
                    // let id = this.props.posts[post].id                    
                    return <Post 
                                post={this.props.posts[post]} key={post} 
                                eliminarPost={this.props.eliminarPost}/>
                    // return <Post post={id} key={post} />
                } ) }
            </React.Fragment>
        )
    }

    render() {
        return (
            <div className='col-12 col-md-8'>
                {/* <h2 className='text-center'> Posts </h2> */}
                <table className='table'>
                    <thead>
                        <tr>
                            <th> ID                     </th>
                            <th> Post                   </th>
                            <th colSpan='3'> Acciones   </th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.showLista() }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Posts;