import React, { Component } from 'react';

class FormularioCrear extends Component {

    titulo      = React.createRef();
    contenido   = React.createRef();

    crearPost = (e) => {
        e.preventDefault();
        let titulo      = this.titulo.current.value;
        let contenido   = this.contenido.current.value;
        let post = {
            title: titulo,
            body: contenido,
            userId: 1
        }
        this.props.crearPost( post );
        e.currentTarget.reset();
    }

    render() {
        return (
            <form className='col-8' onSubmit={this.crearPost}>
                <legend className='text-center'> Crear nuevo post </legend>
                <div className='form-group'>
                    <label> Titulo: </label>
                    <input ref={this.titulo} type='text' placeholder='Título del post' className='form-control' />
                </div>
                <div className='form-group'>
                    <label> Contenido: </label>
                    <textarea ref={this.contenido} className='form-control' placeholder='Contenido del post...'></textarea>
                </div>
                <input type='submit' className='btn btn-primary' value='Crear' />
            </form>
        );
    }
}

export default FormularioCrear;