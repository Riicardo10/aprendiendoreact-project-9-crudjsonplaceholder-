import React    from 'react';
import {Link}   from 'react-router-dom'
import '../styles/navegacion.css';

const Header = () => {
    return (
        <nav className='col-12 col-md-8 navegacion'>
            <Link className='link' to={'/'}>         Todos los posts </Link>
            <Link className='link' to={'/crear'}>    Crear post </Link>
        </nav>
    );
}

export default Header;