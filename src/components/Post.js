import React, {Component}   from 'react';
import {Link}               from 'react-router-dom'
import swal                 from 'sweetalert2';

class Post extends Component {

    confirmEliminar = () => {
        let {id} = this.props.post;
        swal({
            title: 'Está seguro de eliminar el registro?',
            text: "Esta acción no se puede revertir!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, deseo eliminarlo'
        }).then((result) => {
            if (result.value) {
                this.props.eliminarPost( id )
                swal(
                    'Eliminado!',
                    'El registro ' + id + ' ha sido eliminado.',
                    'success'
                )
            }
        })
    }

    render() {
        let {id, title} = this.props.post;
        return (
            <React.Fragment>
            <tr>
                <td> {id}    </td>
                <td> {title} </td>
                <td>
                    <Link className='btn btn-primary btn-sm' to={'/post/' + id}> Ver </Link>
                </td>
                <td>
                    <Link className='btn btn-info btn-sm' to={'/post/' + id + '/edit'}> Editar </Link>
                </td>
                <td>
                    {/* <input onClick={ () => props.eliminarPost(id) } type='button' className='btn btn-danger btn-sm' value='Eliminar' /> */}
                    <input onClick={ this.confirmEliminar } type='button' className='btn btn-danger btn-sm' value='Eliminar' />
                </td>
            </tr>
            </React.Fragment>
        );
    }
}

export default Post;