import React, { Component }             from 'react';
import {BrowserRouter, Route, Switch}   from 'react-router-dom'
import axios                            from 'axios';
import swal                             from 'sweetalert2';
import Header           from '../components/Header';
import Navegacion       from '../components/Navegacion';
import ListaPost        from '../components/ListaPost';
import DetallePost      from '../components/DetallePost';
import FormularioCrear  from '../components/FormularioCrear';
import FormularioEditar from '../components/FormularioEditar';

class Router extends Component {

    state = {
        posts: []
    }

    componentDidMount() {
        this.getPosts();
    }

    getPosts = () => {
        axios.get( 'https://jsonplaceholder.typicode.com/posts' )
            .then( res => {
                this.setState( {posts: res.data} );
            } )
            .catch( err => {
                console.log(err)
            });
    }

    eliminarPost = (id) => {
        axios.delete( 'https://jsonplaceholder.typicode.com/posts/' + id )
            .then( res => {
                if (res.status === 200) {
                    const posts     = [...this.state.posts];
                    let resultado   = posts.filter( post => {
                        return post.id !== id;
                    } )
                    this.setState( {posts: resultado} );
                }
            } )
            .catch( err => {
                console.log(err);
            } )
    }

    crearPost = (post) => {
        axios.post( 'https://jsonplaceholder.typicode.com/posts', {post} )
            .then( res => {
                if( res.status === 201 ){
                    let id = {id: res.data.id};
                    const nuevo = Object.assign( {}, res.data.post, id );
                    this.setState( prevState => ( {
                        posts: [...prevState.posts, nuevo]
                    } ) );
                    swal(
                        'Registro creado!',
                        'Se ha creado el post exitosamente!',
                        'success'
                      )
                }
                // window.location.href = "/"
            } )
            .catch( err => {
                console.log(err);
            } );
    }

    editarPost = (post_data) => {
        axios.put( 'https://jsonplaceholder.typicode.com/posts/' + post_data.id, {post_data} )
            .then( res => {
                if( res.status === 200 ){
                    // this.getPosts()
                    let id        = post_data.id;
                    const posts   = [...this.state.posts];
                    let post_editar    = posts.findIndex( item => {
                        return id === item.id
                    } );
                    console.log(post_editar);
                    posts[post_editar] = post_data;
                    
                    this.setState( {posts: posts} );
                    swal(
                        'Registro actualizado!',
                        'Se ha editado el post exitosamente!',
                        'success'
                    )
                }
                // window.location.href = "/"
            } )
            .catch( err => {
                console.log(err);
            } );
    }

    render() {
        return (
            <BrowserRouter>
                <div className='container'>
                    <div className='row justify-content-center'>
                        <Header />
                        <Navegacion />
                        <Switch>
                            <Route exact path='/'               render={ this.showPosts }>  </Route>    
                            <Route exact path='/post/:id'       render={ this.showPost  }>  </Route>    
                            <Route exact path='/crear'          render={ this.createPost }>  </Route>   
                            <Route exact path='/post/:id/edit'   render={ this.editPost  }>  </Route>     
                        </Switch>
                    </div>
                </div>
            </BrowserRouter>
        );
    }

    editPost = (props) => {
        let id      = props.match.params.id;
        let posts   = [...this.state.posts];
        let post = posts.filter( post => {
            return post.id === Number(id);
        } );
        return (
            <FormularioEditar 
                    post={post[0]}
                    editarPost={this.editarPost} />
        );
    }

    showPosts = () => {
        return <ListaPost 
                    posts={this.state.posts}
                    eliminarPost={this.eliminarPost} />
    }

    showPost = (props) => {
        let id      = props.match.params.id;
        let posts   = [...this.state.posts];
        let post = posts.filter( post => {
            return post.id === Number(id);
        } );
        return (
            <DetallePost post={post[0]} />
        );
    }

    createPost = () => {
        return <FormularioCrear 
                    crearPost={this.crearPost} />
    }

}

export default Router;